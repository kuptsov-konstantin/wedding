﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WeddingMenu.Startup))]
namespace WeddingMenu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
