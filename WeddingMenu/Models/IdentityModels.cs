﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WeddingMenu.DatabaseModels;

namespace WeddingMenu.Models
{
    // В профиль пользователя можно добавить дополнительные данные, если указать больше свойств для класса ApplicationUser. Подробности см. на странице https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base()
        {
            Orders = new HashSet<Invitation>();
        }
        public ApplicationUser(string userName) : base(userName)
        {
            Orders = new HashSet<Invitation>();
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
        public virtual ISet<Invitation> Orders { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
#if DEBUG
        public ApplicationDbContext()
            : base("AppHarborDB", throwIfV1Schema: false)
        {
        }
#else
        public ApplicationDbContext()
            : base("AppHarborDB", throwIfV1Schema: false)
        {
        }
#endif
        public DbSet<Menu> Menus{ get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<OrderDish> OrderDishs { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}