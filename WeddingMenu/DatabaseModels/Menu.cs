﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WeddingMenu.Models;

namespace WeddingMenu.DatabaseModels
{
    public class Menu
    {
        public Menu() {
            Dishs = new HashSet<Dish>();
        }

        [Key]
        public string ID { get; set; }

        public virtual ISet<Dish> Dishs { get; set; }
    }
    public class DishType
    {
        public DishType()
        {
            Dishs = new HashSet<Dish>();
        }
        [Key]
        public string ID { get; set; }
        public string Name { get; set; }
        public ISet<Dish> Dishs { get; set; }
    }
    public class Dish
    {
        [Key]
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Weight { get; set; }
        public string IconUrl { get; set; }
        public virtual Menu AllMenu { get; set; }
        public virtual DishType DishType { get; set; }
    }
    public class OrderDish
    {
        [Key]
        public string ID { get; set; }
        public virtual Dish Dish { get; set; }
        public int Count { get; set; }
    }
    public class Invitation
    {
        public Invitation()
        {
            OrderDishes = new HashSet<OrderDish>();
        }
        [Key]
        public string ID { get; set; }
        public string InvitationMessage { get; set; }
       // public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ISet<OrderDish> OrderDishes { get; set; }
    }

}