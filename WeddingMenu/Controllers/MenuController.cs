﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WeddingMenu.DatabaseModels;
using WeddingMenu.Models;

namespace WeddingMenu.Controllers
{
    public class MenuController : Controller
    {
        private ApplicationDbContext _applicationDbContext = null;
        public ApplicationDbContext applicationDbContext
        {
            get
            {
                return _applicationDbContext ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set { _applicationDbContext = value; }
        }
        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost, Authorize, ValidateAntiForgeryToken, ActionName("SaveNewMenu")]
        public async Task SaveNewMenuAsync(string name, string description, string weight)
        {
            var dish = await applicationDbContext.Dishes.Where(e => e.Name == name).FirstOrDefaultAsync();
            if (dish == null)
            {
                Dish di = new Dish();
                di.Name = name;
                di.Description = description;
                di.Weight = weight;
                di.ID = Guid.NewGuid().ToString();
                applicationDbContext.Dishes.Add(di);
            }
            else
            {
                dish.Description = description;
            }
            await applicationDbContext.SaveChangesAsync();

        }
        [HttpGet, ActionName("GetMenu")]
        public async Task<ActionResult> GetMenuAsync(string id )
        {
            ViewBag.id = id;
            return View();
        }

        [HttpGet, ActionName("GetMenuList")]
        public async Task<JsonResult> GetMenuListAsync()
        {
            var ap = await applicationDbContext.Dishes.OrderBy(or=>or.Name).Select(e => new { @name = e.Name, @description = e.Description, @weight = e.Weight }).ToListAsync();
            return Json(ap, JsonRequestBehavior.AllowGet);
        }
    }
}