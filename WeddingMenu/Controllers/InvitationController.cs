﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WeddingMenu.Models;

namespace WeddingMenu.Controllers
{
    public class InvitationController : Controller
    {
        private ApplicationDbContext _applicationDbContext = null;
        public ApplicationDbContext applicationDbContext
        {
            get
            {
                return _applicationDbContext ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set { _applicationDbContext = value; }
        }
        public InvitationController()
        {

        }
     
        // GET: Invitation
        public async Task<ActionResult> Index(string id)
        {
            //0KPQstCw0LbQsNC10LzQsNGPINCb0Y7QtNC80LjQu9Cw
            //
            string greeting = "";
            DatabaseModels.Invitation inv = await applicationDbContext.Invitations.Where(e => e.ID.Equals(id)).Select(e => e).FirstOrDefaultAsync();
            if (inv != null)
            {
                greeting = inv.InvitationMessage;
            }
     
            byte[] data = Convert.FromBase64String(greeting);
            string decodedString = Encoding.UTF8.GetString(data);
            ViewBag.Greeting = decodedString;
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken, ActionName("SaveInvitation")]
        public async Task SaveInvitationAsync(string id, string encode)
        {          

            DatabaseModels.Invitation inv =  await applicationDbContext.Invitations.Where(e => e.ID.Equals(id)).Select(e => e).FirstOrDefaultAsync();
            if (inv == null)
            {
                inv = new DatabaseModels.Invitation();
                inv.ID = id;
                applicationDbContext.Invitations.Add(inv);
            }            
            inv.InvitationMessage = encode;
            await applicationDbContext.SaveChangesAsync();

        }

        [HttpGet, Authorize, ActionName("GetInvitationList")]
        public async Task<JsonResult> GetInvitationListAsync()
        {
            var ap = await applicationDbContext.Invitations.Select(e => new { @invitationMessage = e.InvitationMessage, @id =e.ID }).ToListAsync();
            return Json(ap, JsonRequestBehavior.AllowGet);
        }
    }
}