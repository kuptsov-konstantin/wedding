namespace WeddingMenu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mig3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dishes", "Weight", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dishes", "Weight");
        }
    }
}
