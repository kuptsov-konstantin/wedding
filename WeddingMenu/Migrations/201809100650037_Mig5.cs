namespace WeddingMenu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mig5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DishTypes",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Dishes", "DishType_ID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Dishes", "DishType_ID");
            AddForeignKey("dbo.Dishes", "DishType_ID", "dbo.DishTypes", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dishes", "DishType_ID", "dbo.DishTypes");
            DropIndex("dbo.Dishes", new[] { "DishType_ID" });
            DropColumn("dbo.Dishes", "DishType_ID");
            DropTable("dbo.DishTypes");
        }
    }
}
