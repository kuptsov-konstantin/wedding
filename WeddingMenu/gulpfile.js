﻿/// <binding />
///
// include plug-ins
const gulp = require('gulp');
const watch = require('gulp-watch');
const gutil = require('gulp-util');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const del = require('del');
const cleanCSS = require('gulp-clean-css');
const bower = require('gulp-bower');
const sourcemaps = require('gulp-sourcemaps');
const modernizr = require('gulp-modernizr');
const rename = require("gulp-rename");
const autoprefixer = require('autoprefixer');
const gulp_autoprefixer = require('gulp-autoprefixer');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const cssbeautify = require('gulp-cssbeautify');

var debug = false;

var scripts = 'Scripts';
var content = 'Content';
var images = 'images';
var scriptFolder = scripts + '/dist/';
var contentFolder = content + '/';
var imagesFolder = images + '/';
//Trumbowyg
//summernote
var config = {
    //JavaScript files that will be combined into a jquery bundle
    jquerysrc: {
        min: [
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/jquery-migrate/jquery-migrate.min.js'
        ],
        max: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/jquery-migrate/jquery-migrate.js']
    },
    jquerybundle: {
        min: 'jquery-bundle.min.js',
        max: 'jquery-bundle.js'
    },


    jqueryvalsrc: {
        min: [
            'bower_components/jquery-validation/dist/jquery.validate.min.js',
            'bower_components/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js',
            'bower_components/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.min.js'

        ],
        max: [
            'bower_components/jquery-validation/dist/jquery.validate.js',
            'bower_components/jquery-validation-unobtrusive/jquery.validate.unobtrusive.js',
            'bower_components/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.js'
        ]
    },

    jqueryvalbundle: {
        min: 'jquery-val-bundle.min.js',
        max: 'jquery-val-bundle.js'
    },

    jqueryuisrc: {
        min: [
            'bower_components/jquery-ui/jquery-ui.min.js'
        ],
        max: [
            'bower_components/jquery-ui/jquery-ui.js'
        ]
    },

    jqueryuibundle: {
        min: 'jquery-ui-bundle.min.js',
        max: 'jquery-ui-bundle.js'
    },

    jqueryuicss: {
        max: [
            'bower_components/jquery-ui/themes/base/core.css',
            'bower_components/jquery-ui/themes/base/resizable.css',
            'bower_components/jquery-ui/themes/base/selectable.css',
            'bower_components/jquery-ui/themes/base/accordion.css',
            'bower_components/jquery-ui/themes/base/autocomplete.css',
            'bower_components/jquery-ui/themes/base/button.css',
            'bower_components/jquery-ui/themes/base/dialog.css',
            'bower_components/jquery-ui/themes/base/slider.css',
            'bower_components/jquery-ui/themes/base/tabs.css',
            'bower_components/jquery-ui/themes/base/datepicker.css',
            'bower_components/jquery-ui/themes/base/progressbar.css',
            'bower_components/jquery-ui/themes/base/theme.css'
        ]
    },

    jqueryuistyle: {
        min: 'jquery-ui-style.min.css',
        max: 'jquery-ui-style.css'
    },

    momentminsrc: {
        min: [
            'bower_components/moment/min/moment-with-locales.min.js',
            'bower_components/moment-timezone/builds/moment-timezone-with-data.min.js'
        ],
        max: [
            'bower_components/moment/min/moment-with-locales.js',
            'bower_components/moment-timezone/builds/moment-timezone-with-data.js'
        ]
    },
    momentbundle: {
        min: 'moment-bundle.min.js',
        max: 'moment-bundle.js'
    },

    //Modernizr
    modernizrsrc: {
        max: ['bower_components/modernizr/src/*.js']
    },
    modernizrbundle: {
        min: 'modernizr.min.js',
        max: 'modernizr.js'
    },


    //JavaScript files that will be combined into a Bootstrap bundle
    bootstrapsrc: {
        min: [
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/respond-minmax/dest/respond.min.js',
            'bower_components/typeahead.js/dist/typeahead.jquery.min.js',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'bower_components/bootstrap-tokenfield/dist/bootstrap-tokenfield.min.js',
            'bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js',
            'bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
            'bower_components/handlebars/handlebars.min.js'
        ],
        max: [
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/respond-minmax/dest/respond.js',
            'bower_components/typeahead.js/dist/typeahead.jquery.js',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'bower_components/bootstrap-tokenfield/dist/bootstrap-tokenfield.js',
            'bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js',
            'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
            'bower_components/handlebars/handlebars.js'
        ]
    },

    //Bootstrap CSS and Fonts
    bootstrapcss: {
        min: [
            'bower_components/bootstrap/dist/css/bootstrap.css',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
            'bower_components/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.min.css',
            'bower_components/bootstrap-tokenfield/dist/css/tokenfield-typeahead.min.css',
            'bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css',
            'bower_components/bootstrap-select/dist/css/bootstrap-select.min.css'
        ],
        max: [
            'bower_components/bootstrap/dist/css/bootstrap.css',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
            'bower_components/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.css',
            'bower_components/bootstrap-tokenfield/dist/css/tokenfield-typeahead.css',
            'bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css',
            'bower_components/bootstrap-select/dist/css/bootstrap-select.css'
        ]
    },
    bootstrapstyle: {
        min: 'bootstrap-style.min.css',
        max: 'bootstrap-style.css'
    },
    //Bootstrap JS
    bootstrapbundle: {
        min: 'bootstrap-bundle.min.js',
        max: 'bootstrap-bundle.js'
    },



    //JavaScript files that will be combined into a Bootstrap bundle
    summernotesrc: {
        min: [
            'bower_components/summernote/dist/summernote.min.js',
            'bower_components/summernote/dist/lang/summernote-ru-RU.min.js'
        ],
        max: [
            'bower_components/summernote/dist/summernote.js',
            'bower_components/summernote/dist/lang/summernote-ru-RU.js'

        ]
    },

    //Bootstrap CSS and Fonts
    summernotecss: {
        min: [
            'bower_components/summernote/dist/summernote.css'
        ],
        max: [
            'bower_components/summernote/dist/summernote.css'
        ]
    },
    summernotestyle: {
        min: 'summernote-style.min.css',
        max: 'summernote-style.css'
    },
    //Bootstrap JS
    summernotebundle: {
        min: 'summernote-bundle.min.js',
        max: 'summernote-bundle.js'
    },


    summernoteFonts: 'bower_components/summernote/dist/font/*.*',
    boostrapfonts: 'bower_components/bootstrap/dist/fonts/*.*',



    weatherfonts: 'bower_components/weather-icons/font/*.*',
    weathercss: {
        min: ['bower_components/weather-icons/css/weather-icons.min.css', 'bower_components/weather-icons/css/weather-icons-wind.min.css'],
        max: ['bower_components/weather-icons/css/weather-icons.css', 'bower_components/weather-icons/css/weather-icons-wind.css']
    },
    weatherstyle: {
        min: 'font-weather-style.min.css',
        max: 'font-weather-style.css'
    },


    fontawesome: 'bower_components/font-awesome/fonts/*.*',

    fontawesomecss: {
        min: 'bower_components/font-awesome/css/font-awesome.min.css',
        max: 'bower_components/font-awesome/css/font-awesome.css'
    },
    fontawesomestyle: {
        min: 'font-awesome-style.min.css',
        max: 'font-awesome-style.css'
    },

    fullcalendarsrc: {
        min: [
            'bower_components/fullcalendar/dist/fullcalendar.min.js',
            'bower_components/fullcalendar/dist/gcal.min.js',
            'bower_components/fullcalendar/dist/locale-all.js'
        ],
        max: [
            'bower_components/fullcalendar/dist/fullcalendar.js',
            'bower_components/fullcalendar/dist/gcal.js',
            'bower_components/fullcalendar/dist/locale-all.js'
        ]
    },
    fullcalendarbundle: {
        min: 'fullcalendar-bundle.min.js',
        max: 'fullcalendar-bundle.js'
    },
    fullcalendarcss: {
        min: [
            'bower_components/fullcalendar/dist/fullcalendar.min.css'/*,
			'bower_components/fullcalendar/dist/fullcalendar.print.min.css'*/
        ],
        max: [
            'bower_components/fullcalendar/dist/fullcalendar.css'/*,
			'bower_components/fullcalendar/dist/fullcalendar.print.css'*/
        ]
    },
    fullcalendarstyle: {
        min: 'fullcalendar-style.min.css',
        max: 'fullcalendar-style.css'
    },

    /////UPLOADCARE
    uploadcaresrc: {
        min: [
            'bower_components/Uploadcare/uploadcare.full.min.js',
            'bower_components/uploadcare-summernote/uploadcare-summernote.js'
        ],
        max: [
            'bower_components/Uploadcare/uploadcare.full.js',
            'bower_components/uploadcare-summernote/uploadcare-summernote.js'
        ]
    },
    uploadcareimg: [
        'bower_components/Uploadcare/images/*.*'
    ],

    uploadcaresummernoteesrc: {
        min: [
            'bower_components/uploadcare-summernote/uploadcare.js'
        ],
        max: [
            'bower_components/uploadcare-summernote/uploadcare.js'
        ]
    },


    ////FLOT
    flotsrc: {
        min: [
            'bower_components/Flot/jquery.flot.js',
            'bower_components/Flot/jquery.flot.pie.js',
            'bower_components/Flot/jquery.flot.time.js',
            'bower_components/Flot/jquery.flot.stack.js',
            'bower_components/Flot/jquery.flot.resize.js',
            'bower_components/flot.curvedlines/curvedLines.js',
            'bower_components/flot.orderbars/js/jquery.flot.orderBars.js',
            'bower_components/flot-spline/js/jquery.flot.spline.min.js'
        ],
        max: [
            'bower_components/Flot/jquery.flot.js',
            'bower_components/Flot/jquery.flot.pie.js',
            'bower_components/Flot/jquery.flot.time.js',
            'bower_components/Flot/jquery.flot.stack.js',
            'bower_components/Flot/jquery.flot.resize.js',
            'bower_components/flot.curvedlines/curvedLines.js',
            'bower_components/flot.orderbars/js/jquery.flot.orderBars.js',
            'bower_components/flot-spline/js/jquery.flot.spline.js'
        ]
    },
    flotbundle: {
        min: 'flot-bundle.min.js',
        max: 'flot-bundle.js'
    },

    ////VENDORS
    vendorssrc: {
        min: [
            'bower_components/echarts/dist/echarts.min.js',
            'bower_components/fastclick/lib/fastclick.js',
            'bower_components/nprogress/nprogress.js',
            'bower_components/bootstrap-progressbar/bootstrap-progressbar.min.js',
            'bower_components/iCheck/icheck.min.js',
            'bower_components/jquery-treegrid/js/jquery.treegrid.min.js',
            'bower_components/jquery-treegrid/js/jquery.treegrid.bootstrap3.js',
            'bower_components/base64-js/base64js.min.js',
            'bower_components/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
            'bower_components/switchery/dist/switchery.min.js',
            'bower_components/chart.js/dist/Chart.bundle.min.js',
            'bower_components/color-conv/index.js',
            'bower_components/bootstrap-daterangepicker/daterangepicker.js',
            //'bower_components/skycons/skycons.js'
            //'bower_components/lightbox2/dist/js/lightbox.min.js',
            //'bower_components/lightbox2/dist/js/lightbox-plus-jquery.min.js'
        ],
        max: [
            'bower_components/echarts/dist/echarts.js',
            'bower_components/fastclick/lib/fastclick.js',
            'bower_components/nprogress/nprogress.js',
            'bower_components/bootstrap-progressbar/bootstrap-progressbar.js',
            'bower_components/iCheck/icheck.js',
            'bower_components/jquery-treegrid/js/jquery.treegrid.js',
            'bower_components/jquery-treegrid/js/jquery.treegrid.bootstrap3.js',
            'bower_components/base64-js/base64js.min.js',
            'bower_components/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
            'bower_components/switchery/dist/switchery.js',
            'bower_components/chart.js/dist/Chart.bundle.js',
            'bower_components/color-conv/index.js',
            'bower_components/bootstrap-daterangepicker/daterangepicker.js',
            //	'bower_components/skycons/skycons.js'
            //'bower_components/lightbox2/dist/js/lightbox.js',
            //'bower_components/lightbox2/dist/js/lightbox-plus-jquery.js'
        ]
    },
    vendorscss: {
        min: [
            'bower_components/nprogress/nprogress.css',
            'bower_components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
            'bower_components/iCheck/skins/flat/green.css',
            'bower_components/jquery-treegrid/css/jquery.treegrid.css',
            'bower_components/jQuery-Smart-Wizard/styles/smart_wizard.css',
            'bower_components/switchery/dist/switchery.min.css',
            'bower_components/bootstrap-daterangepicker/daterangepicker.css',
            'bower_components/animated-climacons/css/animated-climacons.min.css'
            //'bower_components/lightbox2/dist/css/lightbox.min.css'
        ],
        max: [
            'bower_components/nprogress/nprogress.css',
            'bower_components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.css',
            'bower_components/iCheck/skins/flat/green.css',
            'bower_components/jquery-treegrid/css/jquery.treegrid.css',
            'bower_components/jQuery-Smart-Wizard/styles/smart_wizard.css',
            'bower_components/switchery/dist/switchery.css',
            'bower_components/bootstrap-daterangepicker/daterangepicker.css',
            'bower_components/animated-climacons/css/animated-climacons.css'
            //'bower_components/lightbox2/dist/css/lightbox.css'
        ]
    },

    vendorimages: [
        'bower_components/iCheck/skins/flat/green.png',
        'bower_components/iCheck/skins/flat/green@2x.png'
        //'bower_components/lightbox2/dist/images/next.png',
        //'bower_components/lightbox2/dist/images/prev.png',
        //'bower_components/lightbox2/dist/images/close.png',
        //'bower_components/lightbox2/dist/images/loading.gif'
    ],

    vendorsstyle: {
        min: 'vendors-style.min.css',
        max: 'vendors-style.css'
    },
    vendorsbundle: {
        min: 'vendors-bundle.min.js',
        max: 'vendors-bundle.js'
    },

    appcss: {
        in: [
            contentFolder + 'work/' + 'smanager.scss',
            contentFolder + 'work/' + 'font-add.scss',
            contentFolder + 'work/' + 'tags.scss',
            contentFolder + 'work/' + 'option.scss',
            contentFolder + 'work/' + 'mail.scss',
            contentFolder + 'work/' + 'progressbar.scss'

        ],
        out: 'site.css'
    },

    fontsout: ['fonts/', 'dist/fonts/', 'dist/font/'],

    cssout: 'dist/css/',
    images: 'dist/'
};

var settings = {
    "classPrefix": "",
    "options": [
        "addTest",
        "atRule",
        "domPrefixes",
        "hasEvent",
        "html5shiv",
        "html5printshiv",
        "load",
        "mq",
        "prefixed",
        "prefixes",
        "prefixedCSS",
        "setClasses",
        "testAllProps",
        "testProp",
        "testStyles"
    ],
    "feature-detects": [
        "a/download",
        "ambientlight",
        "applicationcache",
        "audio",
        "audio/loop",
        "audio/preload",
        "audio/webaudio",
        "battery",
        "battery/lowbattery",
        "blob",
        "canvas",
        "canvas/blending",
        "canvas/todataurl",
        "canvas/winding",
        "canvastext",
        "contenteditable",
        "contextmenu",
        "cookies",
        "cors",
        "crypto",
        "crypto/getrandomvalues",
        "css/all",
        "css/animations",
        "css/appearance",
        "css/backdropfilter",
        "css/backgroundblendmode",
        "css/backgroundcliptext",
        "css/backgroundposition-shorthand",
        "css/backgroundposition-xy",
        "css/backgroundrepeat",
        "css/backgroundsize",
        "css/backgroundsizecover",
        "css/borderimage",
        "css/borderradius",
        "css/boxshadow",
        "css/boxsizing",
        "css/calc",
        "css/checked",
        "css/chunit",
        "css/columns",
        "css/cubicbezierrange",
        "css/displayrunin",
        "css/displaytable",
        "css/ellipsis",
        "css/escape",
        "css/exunit",
        "css/filters",
        "css/flexbox",
        "css/flexboxlegacy",
        "css/flexboxtweener",
        "css/flexwrap",
        "css/fontface",
        "css/generatedcontent",
        "css/gradients",
        "css/hairline",
        "css/hsla",
        "css/hyphens",
        "css/invalid",
        "css/lastchild",
        "css/mask",
        "css/mediaqueries",
        "css/multiplebgs",
        "css/nthchild",
        "css/objectfit",
        "css/opacity",
        "css/overflow-scrolling",
        "css/pointerevents",
        "css/positionsticky",
        "css/pseudoanimations",
        "css/pseudotransitions",
        "css/reflections",
        "css/regions",
        "css/remunit",
        "css/resize",
        "css/rgba",
        "css/scrollbars",
        "css/shapes",
        "css/siblinggeneral",
        "css/subpixelfont",
        "css/supports",
        "css/target",
        "css/textalignlast",
        "css/textshadow",
        "css/transforms",
        "css/transforms3d",
        "css/transformstylepreserve3d",
        "css/transitions",
        "css/userselect",
        "css/valid",
        "css/vhunit",
        "css/vmaxunit",
        "css/vminunit",
        "css/vwunit",
        "css/will-change",
        "css/wrapflow",
        "custom-protocol-handler",
        "customevent",
        "dart",
        "dataview-api",
        "dom/classlist",
        "dom/createElement-attrs",
        "dom/dataset",
        "dom/documentfragment",
        "dom/hidden",
        "dom/microdata",
        "dom/mutationObserver",
        "elem/datalist",
        "elem/details",
        "elem/output",
        "elem/picture",
        "elem/progress-meter",
        "elem/ruby",
        "elem/template",
        "elem/time",
        "elem/track",
        "elem/unknown",
        "emoji",
        "es5/array",
        "es5/date",
        "es5/function",
        "es5/object",
        "es5/specification",
        "es5/strictmode",
        "es5/string",
        "es5/syntax",
        "es5/undefined",
        "es6/array",
        "es6/collections",
        "es6/contains",
        "es6/generators",
        "es6/math",
        "es6/number",
        "es6/object",
        "es6/promises",
        "es6/string",
        "event/deviceorientation-motion",
        "event/oninput",
        "eventlistener",
        "exif-orientation",
        "file/api",
        "file/filesystem",
        "flash",
        "forms/capture",
        "forms/fileinput",
        "forms/fileinputdirectory",
        "forms/formattribute",
        "forms/inputnumber-l10n",
        "forms/placeholder",
        "forms/requestautocomplete",
        "forms/validation",
        "fullscreen-api",
        "gamepad",
        "geolocation",
        "hashchange",
        "hiddenscroll",
        "history",
        "htmlimports",
        "ie8compat",
        "iframe/sandbox",
        "iframe/seamless",
        "iframe/srcdoc",
        "img/apng",
        "img/jpeg2000",
        "img/jpeg2000",
        "img/jpegxr",
        "img/sizes",
        "img/srcset",
        "img/webp",
        "img/webp-alpha",
        "img/webp-animation",
        "img/webp-lossless",
        "indexeddb",
        "indexeddbblob",
        "input",
        "input/formaction",
        "input/formenctype",
        "input/formmethod",
        "input/formtarget",
        "inputsearchevent",
        "inputtypes",
        "intl",
        "json",
        "lists-reversed",
        "mathml",
        "network/beacon",
        "network/connection",
        "network/eventsource",
        "network/fetch",
        "network/xhr-responsetype",
        "network/xhr-responsetype-arraybuffer",
        "network/xhr-responsetype-blob",
        "network/xhr-responsetype-document",
        "network/xhr-responsetype-json",
        "network/xhr-responsetype-text",
        "network/xhr2",
        "notification",
        "pagevisibility-api",
        "performance",
        "pointerevents",
        "pointerlock-api",
        "postmessage",
        "proximity",
        "queryselector",
        "quota-management-api",
        "requestanimationframe",
        "script/async",
        "script/defer",
        "serviceworker",
        "speech/speech-recognition",
        "speech/speech-synthesis",
        "storage/localstorage",
        "storage/sessionstorage",
        "storage/websqldatabase",
        "style/scoped",
        "svg",
        "svg/asimg",
        "svg/clippaths",
        "svg/filters",
        "svg/foreignobject",
        "svg/inline",
        "svg/smil",
        "templatestrings",
        "test/css/scrollsnappoints",
        "test/elem/bdi",
        "test/img/crossorigin",
        "test/ligatures",
        "textarea/maxlength",
        "touchevents",
        "typed-arrays",
        "unicode",
        "unicode-range",
        "url/bloburls",
        "url/data-uri",
        "url/parser",
        "userdata",
        "vibration",
        "video",
        "video/autoplay",
        "video/loop",
        "video/preload",
        "vml",
        "web-intents",
        "webanimations",
        "webgl",
        "webgl/extensions",
        "webrtc/datachannel",
        "webrtc/getusermedia",
        "webrtc/peerconnection",
        "websockets",
        "websockets/binary",
        "window/framed",
        "workers/blobworkers",
        "workers/dataworkers",
        "workers/sharedworkers",
        "workers/transferables",
        "workers/webworkers",
        "xdomainrequest"
    ]

};

function updateUserCss() {
    return gulp.src(config.appcss.in)
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer({ browsers: ["> 0%"] })]))
        .pipe(concat(config.appcss.out))
        .pipe(cssbeautify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(contentFolder + 'build'));
}

gulp.task('auto-css', () => updateUserCss());
gulp.task('auto-css-watch', () => watch(config.appcss.in, () => updateUserCss()));

gulp.task('sass', () =>
    gulp.src('bower_components/gentelella/src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer({ browsers: ["> 0%"] })]))
        .pipe(concat('gentelella.css'))
        .pipe(cssbeautify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(contentFolder + 'build'))
);

// Synchronously delete the output script file(s)
gulp.task('clean-vendor-scripts', function (cb) {
    debug = debug || false;
    return del([scriptFolder + config.jquerybundle.max,
    scriptFolder + config.bootstrapbundle.max,
    scriptFolder + config.modernizrbundle.max,
    scriptFolder + config.jqueryvalbundle.max,
    scriptFolder + config.jqueryuibundle.max,
    scriptFolder + config.momentbundle.max,
    scriptFolder + config.vendorsbundle.max,
    scriptFolder + config.fullcalendarbundle.max,
    scriptFolder + config.summernotebundle.max,
    scriptFolder + 'uploadcare'
    ], cb);
});

//Create a jquery bundled file
gulp.task('jquery-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    return gulp.src(config.jquerysrc.max)
        .pipe(sourcemaps.init())
        .pipe(concat(config.jquerybundle.max))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(scriptFolder));
});

//Create a moment bundled file
gulp.task('moment-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    return gulp.src(config.momentminsrc.max).pipe(concat(config.momentbundle.max)).pipe(gulp.dest(scriptFolder));
});

//Create a jquery validate bundled file
gulp.task('jquery-val-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    return gulp.src(config.jqueryvalsrc.max).pipe(concat(config.jqueryvalbundle.max)).pipe(gulp.dest(scriptFolder));
});

//Create a jquery ui bundled file
gulp.task('jquery-ui-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    if (debug === true) {
        return gulp.src(config.jqueryuisrc.max)
            .pipe(sourcemaps.init())
            .pipe(concat(config.jqueryuibundle.max))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest(scriptFolder));
    } else {
        return gulp.src(config.jqueryuisrc.max)
            .pipe(concat(config.jqueryuibundle.max))
            .pipe(gulp.dest(scriptFolder));
    }
});

//Create a vendors fullcalendar validate bundled file
gulp.task('vendors-fullcalendar', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    if (debug === true) {
        return gulp.src(config.fullcalendarsrc.max)
            .pipe(sourcemaps.init())
            .pipe(concat(config.fullcalendarbundle.max))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest(scriptFolder));
    } else {
        return gulp.src(config.fullcalendarsrc.max)
            .pipe(concat(config.fullcalendarbundle.max))
            .pipe(gulp.dest(scriptFolder));
    }
});


//Create a flot validate bundled file
gulp.task('flot', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    if (debug === true) {
        return gulp.src(config.flotsrc.max)
            .pipe(sourcemaps.init())
            .pipe(concat(config.flotbundle.max))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest(scriptFolder));
    } else {
        return gulp.src(config.flotsrc.max)
            .pipe(concat(config.flotbundle.max))
            .pipe(gulp.dest(scriptFolder));
    }
});



//Create a vendors validate bundled file
gulp.task('vendors', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    if (debug === true) {
        return gulp.src(config.vendorssrc.max)
            .pipe(sourcemaps.init())
            .pipe(concat(config.vendorsbundle.max))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest(scriptFolder));
    } else {
        return gulp.src(config.vendorssrc.max)
            .pipe(concat(config.vendorsbundle.max))
            .pipe(gulp.dest(scriptFolder));
    }
});

gulp.task('vendors-uploadcare-summernote', ['bower-restore'], () => {
    return gulp.src(config.uploadcaresummernoteesrc.max).pipe(rename(function (path) {
        path.basename += "-summernote";
    })).pipe(gulp.dest('bower_components/uploadcare-summernote/'));
});
//Create a vendors uploadcare validate bundled file
gulp.task('vendors-uploadcare', ['clean-vendor-scripts', 'bower-restore', 'vendors-uploadcare-summernote'], function () {
    debug = debug || false;

    return gulp.src(config.uploadcaresrc.max).pipe(gulp.dest(scriptFolder + 'uploadcare'));
});

//Create a vendors validate bundled file
gulp.task('vendors-summernote', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false;
    return gulp.src(config.summernotesrc.max).pipe(concat(config.summernotebundle.max)).pipe(gulp.dest(scriptFolder));
});


//Create a bootstrap bundled file
gulp.task('bootstrap-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    debug = debug || false; if (debug === true) {
        return gulp.src(config.bootstrapsrc.max)
            .pipe(sourcemaps.init())
            .pipe(concat(config.bootstrapbundle.max))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest(scriptFolder));
    } else {
        return gulp.src(config.bootstrapsrc.max)
            .pipe(concat(config.bootstrapbundle.max))
            .pipe(gulp.dest(scriptFolder));
    }
});

//Create a modernizr bundled file
gulp.task('modernizer', ['clean-vendor-scripts', 'bower-restore'], function () {
    gulp.src(config.modernizrsrc.max)
        .pipe(modernizr(settings))
        .pipe(gulp.dest(scriptFolder));
});

// Combine and the vendor files from bower into bundles (output to the Scripts folder)
gulp.task('vendor-scripts', ['jquery-bundle',
    'bootstrap-bundle', 'modernizer',
    'moment-bundle', 'jquery-val-bundle',
    'jquery-ui-bundle', 'vendors', 'flot',
    'vendors-fullcalendar',
    'vendors-summernote', 'vendors-uploadcare'], function () {
        debug = debug || false;
        return;
    });

gulp.task('vendor-images', ['clean-styles', 'bower-restore'], () => {
    return gulp.src(config.vendorimages).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('uploadcare-images', ['clean-styles', 'bower-restore'], () => {
    return gulp.src(config.uploadcareimg).pipe(gulp.dest(imagesFolder + 'uploadcare'));
});

// Combine and the vendor files from bower into bundles (output to the Scripts folder)
gulp.task('images', ['vendor-images', 'uploadcare-images'], function () {
    debug = debug || false;
    return;
});

// Synchronously delete the output style files (css / fonts)
gulp.task('clean-styles', function (cb) {
    debug = debug || false;
    return del([contentFolder + config.fontsout[0], contentFolder + config.fontsout[1], contentFolder + config.cssout, contentFolder + 'build/' + config.appcss.out], cb);
});

gulp.task('css-bootstrap', ['clean-styles', 'bower-restore'], () => {
    debug = debug || false;
    return gulp.src(config.bootstrapcss.max).pipe(concat(config.bootstrapstyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('css-summernote', ['clean-styles', 'bower-restore'], () => {
    debug = debug || false;
    return gulp.src(config.summernotecss.max).pipe(concat(config.summernotestyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('css-fullcalendar', ['clean-styles', 'bower-restore'], () => {
    debug = debug || false;
    return gulp.src(config.fullcalendarcss.max).pipe(concat(config.fullcalendarstyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('css-fontweather', ['clean-styles', 'bower-restore'], () => {
    debug = debug || false;
    return gulp.src(config.weathercss.max).pipe(concat(config.weatherstyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('css-fontawesome', ['clean-styles', 'bower-restore'], () => {
    debug = debug || false;
    return gulp.src(config.fontawesomecss.max).pipe(concat(config.fontawesomestyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('css-jqueryui', ['clean-styles', 'bower-restore'], () => {
    debug = debug || false;
    return gulp.src(config.jqueryuicss.max).pipe(concat(config.jqueryuistyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('css', ['clean-styles', 'bower-restore'], function () {
    debug = debug || false;
    return gulp.src(config.vendorscss.max).pipe(concat(config.vendorsstyle.max)).pipe(gulp.dest(contentFolder + config.cssout));
});

gulp.task('fonts', ['clean-styles', 'bower-restore'], function () {
    debug = debug || false;
    return gulp.src([config.boostrapfonts, config.fontawesome, config.summernoteFonts, config.weatherfonts])
        .pipe(gulp.dest(contentFolder + config.fontsout[0]))
        .pipe(gulp.dest(contentFolder + config.fontsout[1]))
        .pipe(gulp.dest(contentFolder + config.fontsout[2]));
});

// Combine and minify css files and output fonts
gulp.task('styles', ['auto-css', 'css', 'css-summernote', 'css-fullcalendar', 'css-bootstrap', 'css-jqueryui', 'css-fontawesome', 'css-fontweather', 'fonts', 'images'], function () {
    debug = debug || false;

});

//Restore all bower packages
gulp.task('bower-restore', function () {
    debug = debug || false;
    return bower();
});

//Set a default tasks
gulp.task('updateAll:release', ['vendor-scripts', 'styles'], function () {
    debug = debug || false;

});
gulp.task('updateAll:debug', ['debug', 'vendor-scripts', 'styles'], function () {
    debug = debug || false;

});

gulp.task('debug', function () {
    debug = true;
    gutil.log(gutil.colors.green('RUNNING IN DEBUG MODE'));
    return gulp.start('clean-vendor-scripts');
});
