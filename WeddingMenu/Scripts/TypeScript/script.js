$("#menuBtn").click(function () {
    console.log("ready!");
});
var GUID = /** @class */ (function () {
    function GUID() {
    }
    GUID.getNumber = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    GUID.getGUID = function () {
        return (GUID.getNumber() + GUID.getNumber() + "-" + GUID.getNumber() + "-4" + GUID.getNumber().substr(0, 3) + "-" + GUID.getNumber() + "-" + GUID.getNumber() + GUID.getNumber() + GUID.getNumber()).toLowerCase();
    };
    GUID.create = function (id) {
        if (id === void 0) { id = null; }
        var guid1 = new GUID();
        if (id == null) {
            guid1.guid = GUID.getGUID();
        }
        else {
            guid1.guid = id;
        }
        return guid1;
    };
    return GUID;
}());
var Menu = /** @class */ (function () {
    function Menu() {
    }
    Menu.prototype.initMenu = function (url) {
        $("#button-addon2").on('click', function () {
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    name: $('#inputText').val(),
                    weight: $('#inputText3').val(),
                    description: $('#inputText2').val(),
                },
                success: function (result) {
                    Menu.loadMenu(Menu.urlMenu);
                    $('#inputText').val("");
                    $('#inputText3').val("");
                    $('#inputText2').val("");
                }
            });
            return false;
        });
    };
    Menu.loadMenu = function (url) {
        Menu.urlMenu = url;
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $('#listId').html('');
                for (var i = 0; i < result.length; i++) {
                    var f = result[i];
                    var strT = "<div class='list-group-item list-group-item-action flex-column align-items-start'><div class='d-flex w-100 justify-content-between'>" +
                        "<h5 class='mb-1'>" + f.name + "</h5>" +
                        "<small>" + f.weight + "</small>" +
                        " </div>" +
                        "<p class='mb-1'>" + f.description + "</p>" +
                        " <small>Сылка</small>" +
                        "  </div>";
                    $('#listId').append(strT);
                    // f.id;
                }
                //console.log(result);
                // $("#div1").html(result);
            }
        });
    };
    return Menu;
}());
var Invitation = /** @class */ (function () {
    function Invitation() {
        this.id = null;
    }
    Invitation.createURL = function (encode, guid) {
        return '/Invitation/Index?&id=' + guid;
    };
    Invitation.createMenuURL = function (guid) {
        return '/Menu/GetMenu?id=' + guid;
    };
    Invitation.prototype.initStatistics = function (url) {
        $('#inputText').on('keyup', function () {
            var encodedData = window.btoa(unescape(encodeURIComponent($('#inputText').val())));
            console.log(encodedData);
            var id = GUID.getGUID();
            $('#inviteText').data('encode', encodedData);
            $('#inviteText').data('id', encodedData);
            //$('#inviteText').text(window.location.origin + r);
        });
        $("#button-addon2").on('click', function () {
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    encode: $('#inviteText').data('encode'),
                    id: $('#inviteText').data('id')
                },
                success: function (result) {
                    Invitation.loadInvitations(Invitation.urlInvitation);
                }
            });
            return false;
        });
    };
    Invitation.loadInvitations = function (url) {
        Invitation.urlInvitation = url;
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $('#listId').html('');
                for (var i = 0; i < result.length; i++) {
                    var f = result[i];
                    var url = Invitation.createURL(f.invitationMessage, f.id);
                    var txt = decodeURIComponent(escape(atob(f.invitationMessage)));
                    var strT = "<div class='list-group-item list-group-item-action flex-column align-items-start'><div class='d-flex w-100 justify-content-between'>" +
                        "<h5 class='mb-1'>" + txt + "</h5>" +
                        "<small>" + Invitation.getButtons(GUID.create(f.id)) + "</small>" +
                        " </div>" +
                        "<p class='mb-1'> Пригласительное: " + window.location.origin + url + "<br/>Меню: " +
                        window.location.origin + Invitation.createMenuURL(f.id) + "</p>" +
                        " <small>Сылка</small>" +
                        "  </div>";
                    $('#listId').append(strT);
                    // f.id;
                }
                //console.log(result);
                // $("#div1").html(result);
            }
        });
    };
    Invitation.getButtons = function (id) {
        return "<div class='btn-group' role='group' aria-label='Basic example'>" +
            "<button type='button' class='btn btn-secondary' onclick=\"Invitation.prototype.edit('" + id.guid + "')\">Редактировать</button>" +
            "<button type='button' class='btn btn-danger' onclick=\"Invitation.prototype.remove('" + id.guid + "')\">Удалить</button>" +
            "</div>";
    };
    Invitation.prototype.loadEat = function (url) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $('#eat-menu').html('');
                for (var i = 0; i < result.length; i++) {
                    var f = result[i];
                    var strT = "<div class='list-group-item list-group-item-action flex-column align-items-start'><div class='d-flex w-100 justify-content-between'>" +
                        "<h2 class='mb-1'>" + f.name + "</h2>" +
                        "<small>" + f.weight + "</small>" +
                        "</div>" +
                        "<h3 class='mb-1'>" + f.description + "</h3>" +
                        " <small></small>" +
                        "</div>";
                    $('#eat-menu').append(strT);
                    // f.id;
                }
                //console.log(result);
                // $("#div1").html(result);
            }
        });
    };
    Invitation.prototype.edit = function (id) {
        console.log(id);
    };
    Invitation.prototype.remove = function (id) {
        console.log(id);
    };
    return Invitation;
}());
//# sourceMappingURL=script.js.map