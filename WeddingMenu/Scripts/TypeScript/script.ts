﻿$("#menuBtn").click(function () {
    console.log("ready!");
});

class GUID {

    guid: string;
    constructor() {

    }
    static getNumber(): string {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    static getGUID(): string {
        return (GUID.getNumber() + GUID.getNumber() + "-" + GUID.getNumber() + "-4" + GUID.getNumber().substr(0, 3) + "-" + GUID.getNumber() + "-" + GUID.getNumber() + GUID.getNumber() + GUID.getNumber()).toLowerCase();
    }
    static create(id: string = null): GUID {
        var guid1 = new GUID();
        if (id == null) {
            guid1.guid = GUID.getGUID();
        } else {
            guid1.guid = id;
        }
        return guid1;
    }
}
interface IMenuCallback {
    description: string;
    name: string;
    weight: string;
}


interface IInvitationCallback {
    invitationMessage: string;
    id: string;
}

class Menu {
    initMenu(url: string) {
        $("#button-addon2").on('click', () => {
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    name: $('#inputText').val(),
                    weight: $('#inputText3').val(),
                    description: $('#inputText2').val(),
                },
                success: function (result) {
                    Menu.loadMenu(Menu.urlMenu);
                    $('#inputText').val("");
                    $('#inputText3').val("");
                    $('#inputText2').val("");
                }
            });
            return false;
        });
    }

    static urlMenu: string;
    static loadMenu(url: string) {
        Menu.urlMenu = url;
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result: Array<IMenuCallback>) {
                $('#listId').html('');
                for (var i = 0; i < result.length; i++) {
                    var f = result[i];

                    var strT = "<div class='list-group-item list-group-item-action flex-column align-items-start'><div class='d-flex w-100 justify-content-between'>" +
                        "<h5 class='mb-1'>" + f.name + "</h5>" +
                        "<small>" + f.weight + "</small>" +
                        " </div>" +
                        "<p class='mb-1'>" + f.description + "</p>" +
                        " <small>Сылка</small>" +
                        "  </div>";
                    $('#listId').append(strT);


                    // f.id;
                }
                //console.log(result);
                // $("#div1").html(result);
            }
        });
    }
}

class Invitation {


    static createURL(encode: string, guid: string): string {
        return '/Invitation/Index?&id=' + guid;
    }
    static createMenuURL(guid: string): string {
        return '/Menu/GetMenu?id=' + guid;
    }


    initStatistics(url: string) {
        $("#button-addon2").on('click', () => {
            var form = $('#__AjaxAntiForgeryForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    encode: window.btoa(unescape(encodeURIComponent($('#inviteText').val()))),
                    id: GUID.getGUID()
                },
                success: function (result) {
                    Invitation.loadInvitations(Invitation.urlInvitation);
                }
            });
            return false;
        });
    }
    static urlInvitation: string;
    static loadInvitations(url: string) {
        Invitation.urlInvitation = url;
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result: Array<IInvitationCallback>) {
                $('#listId').html('');
                for (var i = 0; i < result.length; i++) {
                    var f = result[i]; 
                    var url = Invitation.createURL(f.invitationMessage, f.id);
                    var txt = decodeURIComponent(escape(atob(f.invitationMessage)));
                    var strT = "<div class='list-group-item list-group-item-action flex-column align-items-start'><div class='d-flex w-100 justify-content-between'>" +
                        "<h5 class='mb-1'>" + txt + "</h5>" +
                        "<small>" + Invitation.getButtons(GUID.create(f.id)) + "</small>" +
                        " </div>" +
                        "<p class='mb-1'> Пригласительное: " + window.location.origin+ url + "<br/>Меню: "+
                        window.location.origin + Invitation.createMenuURL(f.id) + "</p>" +
                        " <small>Сылка</small>" +
                        "  </div>";
                    $('#listId').append(strT);


                   // f.id;
                }
                //console.log(result);
                // $("#div1").html(result);
            }
        });
    }
    static getButtons(id: GUID): string {
        return "<div class='btn-group' role='group' aria-label='Basic example'>" +
            "<button type='button' class='btn btn-secondary' onclick=\"Invitation.prototype.edit('" + id.guid + "')\">Редактировать</button>" +
            "<button type='button' class='btn btn-danger' onclick=\"Invitation.prototype.remove('" + id.guid + "')\">Удалить</button>" +
            "</div>";
    }

    loadEat(url: string) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result: Array<IMenuCallback>) {
                $('#eat-menu').html('');
                for (var i = 0; i < result.length; i++) {
                    var f = result[i];

                    var strT = "<div class='list-group-item list-group-item-action flex-column align-items-start'><div class='d-flex w-100 justify-content-between'>" +
                        "<h2 class='mb-1'>" + f.name + "</h2>" +
                        "<small>" + f.weight + "</small>" +
                        "</div>" +
                        "<h3 class='mb-1'>" + f.description + "</h3>" +
                        " <small></small>" +
                        "</div>";
                    $('#eat-menu').append(strT);


                    // f.id;
                }
                //console.log(result);
                // $("#div1").html(result);
            }
        });

    }

    edit(id: string) {
        console.log(id);
    }
    remove(id: string) {
        console.log(id);
    }
}